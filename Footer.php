<!DOCTYPE html>
<html>
<meta charset="utf-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="Css_for_everything.css" hreflang="cs" rel="stylesheet">

    <link rel="stylesheet" href="../dist/css/swiper.min.css">
     <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

     <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

     <link href="https://fonts.googleapis.com/css?family=Rubik:700&display=swap" rel="stylesheet">


<body>

  <div class="container">
    <div class="row">
      <div class="col-4">
        <h2 class="lux">JSME TADY PRO VÁS</h2>
        <p class="naut">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vitae arcu tempor neque lacinia pretium. In convallis. Nullam dapibus fermentum ipsum. Etiam bibendum elit eget erat. </p>
      </div>

      <div class="col-3">
        <h2 class="lux">NAPIŠTE ČI ZAVOLEJTE</h2>
        <p class="naut"><i class="fas fa-phone"></i> +420 602 477 338</p>
        <p class="naut"><i class="fas fa-envelope-square"></i>  info@plynomontaz.cz</p>
      </div>

      <div class="col-5">
        <h2 class="orianna"><div class="fb-page" data-href="https://www.facebook.com/kadlecsoftware/" data-tabs="timeline" data-width="400" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kadlecsoftware/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kadlecsoftware/">Kadlec - Software</a></blockquote></div>
      </div>

    </div>

  </div>

  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v3.3"></script>

<footer>
  <div class="container gragas">
    <div class="row">
      <div class="zabak col-6">
        <h2 class="j4">© Plynomontáž.cz 2019, Provozuje Josef Kadlec</h2>
      </div>
      <div class="col-6">
        <h2 class="garen">Vytvořil Kadlec-Software</h2>        
      </div>
    </div>

  </div>
</footer>

</body>
</html>
